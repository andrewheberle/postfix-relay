FROM registry.gitlab.com/andrewheberle/docker-base:alpine3.18

RUN apk --no-cache add ca-certificates postfix

COPY rootfs /

RUN find /etc/services.d -name run -type f -exec chmod 0755 {} \; && \
    find /etc/services.d -name halt -type f -exec chmod 0755 {} \; && \
    find /etc/cont-init.d -type f -exec chmod 0755 {} \; && \
    find /usr/local/bin -name \*.sh -type f -exec chmod 0755 {} \;

ENV POSTFIX_MYHOSTNAME="localhost" \
    POSTFIX_MYDOMAIN="localdomain" \
    POSTFIX_RELAYHOST="" \
    POSTFIX_MYNETWORKS="" \
    POSTFIX_MYNETWORKS_STYLE="host" \
    POSTFIX_MAILLOG_FILE="/dev/stdout" \
    POSTFIX_SMTP_TLS_CHAIN_FILES="" \
    POSTFIX_SMTP_TLS_SECURITY_LEVEL="" \
    POSTFIX_SMTP_TLS_CIPHERS="medium" \
    POSTFIX_SMTP_TLS_PROTOCOLS="!SSLv2, !SSLv3" \
    POSTFIX_SMTP_TLS_MANDATORY_CIPHERS="medium" \
    POSTFIX_SMTP_TLS_MANDATORY_PROTOCOLS="!SSLv2, !SSLv3" \
    POSTFIX_SMTP_TLS_LOGLEVEL="0" \
    POSTFIX_SMTPD_TLS_CHAIN_FILES="" \
    POSTFIX_SMTPD_TLS_SECURITY_LEVEL="" \
    POSTFIX_SMTPD_TLS_CIPHERS="medium" \
    POSTFIX_SMTPD_TLS_PROTOCOLS="!SSLv2, !SSLv3" \
    POSTFIX_SMTPD_TLS_MANDATORY_CIPHERS="medium" \
    POSTFIX_SMTPD_TLS_MANDATORY_PROTOCOLS="!SSLv2, !SSLv3" \
    POSTFIX_SMTPD_TLS_DH1024_PARAM_FILE="" \
    POSTFIX_SMTPD_TLS_LOGLEVEL="0" \
    POSTFIX_ROOT_ALIAS="/dev/null" \
    POSTFIX_TLS_SERVER_SNI_MAPS="" \
    POSTFIX_TABLES=""
