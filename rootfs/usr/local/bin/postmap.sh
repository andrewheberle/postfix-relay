#!/bin/sh
if [ "${POSTFIX_TABLES}" != "" ]; then
    for TABLE in ${POSTFIX_TABLES}; do
        # split TYPE:/path/to/table
        tbl="$(echo "${TABLE}" | sed -r 's/(.*:)?(.*)$/\2/')"
        
        # skip if table does not exist
        if [ ! -f "${tbl}" ]; then
            continue
        fi

        # split TYPE:/path/to/table
        type="$(echo "${TABLE}" | sed -r -e 's/(.*:)?(.*)$/\1/' -e 's/://')"

        # set to default type
        if [ "${type}" = "" ]; then
            type="$(postconf -h default_database_type)"
        fi

        # work out db extension
        case "${type}" in
            "btree")
                ext="db"
            ;;
            "cdb")
                ext="cdb"
            ;;
            "dbm")
                ext="pag"
            ;;
            "hash")
                ext="db"
            ;;
            "lmdb")
                ext="lmdb"
            ;;
            "sdbm")
                ext="pag"
            ;;
            *)
                ext=""
            ;;
        esac

        # skip non generated files
        if [ "${ext}" = "" ]; then
            continue
        fi

        # run unconditionally if db does not exist
        if [ ! -f "${tbl}.${ext}" ]; then
            postmap "${TABLE}"
            continue
        fi

        # check modification times and run as needed
        if [ "$(stat -L -c '%Y' "${tbl}")" -gt "$(stat -L -c '%Y' "${tbl}.${ext}")" ]; then
            postmap "${TABLE}"
        fi
    done
fi
