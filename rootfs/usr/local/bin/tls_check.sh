#!/bin/sh
# Checks certificate files and reloads postfix if they have changed in the last hour

if [ "${POSTFIX_SMTP_TLS_CHAIN_FILES}" != "" ] || [ "${POSTFIX_SMTP_TLS_CHAIN_FILES}" != "" ]; then
    LIST="$(echo "${POSTFIX_SMTP_TLS_CHAIN_FILES},${POSTFIX_SMTP_TLS_CHAIN_FILES}" | sed 's/,/\n/g' | sort | uniq | xargs)"
    for file in $LIST; do
        if [ $(( $(date '+%s') - $(date --reference "$file" '+%s') )) -le 3600 ]; then
            postfix reload
            break
        fi
    done
fi