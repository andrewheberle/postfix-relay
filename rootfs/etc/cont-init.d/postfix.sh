#!/command/with-contenv sh

# fail on all errors
set -e

# set basic postfix config from env vars
_vars="$(env | \
    grep -E -v '(POSTFIX_TLS_SERVER_SNI_MAPS|POSTFIX_ROOT_ALIAS|POSTFIX_TABLES)' | \
    grep -E '^POSTFIX_.*=.*$' | \
    sed -r 's/^POSTFIX_(.*)/\1/')"
_old_ifs=${IFS}
IFS=$'\n'
for _line in ${_vars}; do
    _setting="$(echo "${_line}" | sed -r 's/^(.*)=.*?$/\1/' | tr '[:upper:]' '[:lower:]')"
    _value="$(echo "${_line}" | sed -r 's/^.*=(.*)?$/\1/')"
    postconf -e "${_setting}"="${_value}"
done
IFS=${_old_ifs}

# set up tls sni support
# this expects the file mounted at ${POSTFIX_TLS_SERVER_SNI_MAPS} to be valid
# as per http://www.postfix.org/postconf.5.html#tls_server_sni_maps
if [ "${POSTFIX_TLS_SERVER_SNI_MAPS}" != "" ]; then
    postconf -e tls_server_sni_maps="\${default_database_type}:${POSTFIX_TLS_SERVER_SNI_MAPS}"
    postmap -F "${POSTFIX_TLS_SERVER_SNI_MAPS}"
fi

# set up root alias
sed -i -r "s/^#?root:.*/root: $(printf '%s\n' "${POSTFIX_ROOT_ALIAS}" | sed 's/[[\.*/]/\\&/g; s/$$/\\&/; s/^^/\\&/')/" /etc/postfix/aliases
newaliases

# enable crond if POSTFIX_TABLES is non-empty
if [ "${POSTFIX_TABLES}" != "" ]; then
    rm -f /etc/services.d/crond/down

    # ensure script is executable
    chmod +x /usr/local/bin/postmap.sh

    # do initial run immediately
    /usr/local/bin/postmap.sh
fi
